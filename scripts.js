/*
 Smooth scrolling for href links
 Weird bug-ish: if i put the chuck of code
 below, the scrolling does not work so 
 as much as possible don't move the code
 https://www.youtube.com/watch?v=9nh0snfJ7Ao
*/
document.querySelectorAll('a[href^="#"]').forEach(anchor => {
  anchor.addEventListener("click", function(e){
    e.preventDefault();
    document.querySelector(this.getAttribute("href")).scrollIntoView({
      behavior: "smooth"
    });
  });
});

function scrollToTop(){
  window.scrollTo(0, 0);
}

var loading = document.querySelector(".loading");
window.addEventListener("load", function(){
  loading.className += " hidden";
  scrollToTop();
})

//MODAL PART
// Get the modal
var modal = document.getElementById("modalquiz");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("pearl");
img.onclick = function(){modal.style.display = "block";}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}

let slideIndex = 1;
showSlides(slideIndex);

// Next/previous controls
function plusSlides(n) {
showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
showSlides(slideIndex = n);
}

function showSlides(n) {
let i;
let slides = document.getElementsByClassName("mySlides");
let dots = document.getElementsByClassName("dot");
if (n > slides.length) {slideIndex = 1}
if (n < 1) {slideIndex = slides.length}
for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
}
for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
}
slides[slideIndex-1].style.display = "block";
dots[slideIndex-1].className += " active";
}

window.onscroll = function() {myFunction()};
//chang pixel value appropriately 
function myFunction() {
  if (document.documentElement.scrollTop > 20) {
    document.getElementById("a_opps").style.animation = "moveRight 1s linear forwards";
    document.getElementById("a_awards1").style.animation = "moveLeft 1s linear forwards";
  }

  if (document.documentElement.scrollTop > 150) {
    document.getElementById("a_interns").style.animation = "moveRight 1s linear forwards";
    document.getElementById("a_awards2").style.animation = "moveLeft 1s linear forwards";
  }

  if (document.documentElement.scrollTop > 400) {
    document.getElementById("CS").style.animation = "moveToLeft 1s linear forwards";
  }
  if (document.documentElement.scrollTop > 700) {
    document.getElementById("Mixer").style.animation = "moveToLeft 1s linear forwards";
  }
  if (document.documentElement.scrollTop > 1000) {
    document.getElementById("Jf").style.animation = "moveToRight 1s linear forwards";
  }
}

function hideinfos() {

  if (document.getElementById("Button").textContent==="Show Info") {
    document.getElementById("Button").textContent="Hide Info";
  } else {
    document.getElementById("Button").textContent="Show Info";
  }

  const collection = document.getElementsByClassName("info");
  for (let i = 0; i < collection.length; i++) {
    if (collection[i].style.visibility === "hidden") {
      collection[i].style.visibility = "visible";
    } else {
      collection[i].style.visibility = "hidden";
    }
  }
}

