window.onscroll = function() {myFunction()};
const body = document.body;
const html = document.documentElement;
const height = Math.max(body.scrollHeight, body.offsetHeight,
  html.clientHeight, html.scrollHeight, html.offsetHeight);


function myFunction() {
  var curr = document.documentElement.scrollTop;
  var perc = curr/height * 100;
  // console.log(perc)

  if ((window.innerHeight + window.pageYOffset) >= document.body.offsetHeight-100) {
      document.getElementById("vis").style.animation = "moveRight 0.5s linear forwards";
      document.getElementById("mis").style.animation = "moveLeft 0.5s linear forwards";
      document.getElementById("values").style.animation = "moveRight 0.5s linear forwards";
      document.getElementById("a_info").style.animation = "moveUp 0.5s linear forwards";
      document.getElementById("a_vid").style.animation = "moveToRight 0.5s linear forwards";
      document.getElementById("a_opps").style.animation = "moveRight 0.5s linear forwards";
      document.getElementById("a_interns").style.animation = "moveRight 0.5s linear forwards";
      document.getElementById("a_awards1").style.animation = "moveToRight 0.5s linear forwards";
      document.getElementById("a_awards2").style.animation = "moveToRight 0.5s linear forwards";
      document.getElementById("CS").style.animation = "moveToLeft 1s linear forwards";
      document.getElementById("Jf").style.animation = "moveToRight 1s linear forwards";
      document.getElementById("Mixer").style.animation = "moveToLeft 1s linear forwards";

      document.getElementById("Button").style.display="block";
      document.getElementById("Button").style.animation="makeVisible 1s linear forwards";
  } else {
    document.getElementById("Button").style.display="none";
  }



  if (document.documentElement.scrollTop > 1) {
    document.getElementById("vis").style.animation = "moveRight 0.5s linear forwards";
  }

  if (document.documentElement.scrollTop > 20) {
    document.getElementById("mis").style.animation = "moveLeft 0.5s linear forwards";
  }

  if (document.documentElement.scrollTop > 40) {
    document.getElementById("values").style.animation = "moveRight 0.5s linear forwards";
  }
  if (document.documentElement.scrollTop > 500) {
    document.getElementById("a_info").style.animation = "moveUp 0.5s linear forwards";
  }
  if (document.documentElement.scrollTop > 700) {
    document.getElementById("a_vid").style.animation = "moveToRight 0.5s linear forwards";
  }

  if (document.documentElement.scrollTop > 1400) {
    document.getElementById("a_opps").style.animation = "moveRight 0.5s linear forwards";
  }

  if (document.documentElement.scrollTop > 1800) {
    document.getElementById("a_interns").style.animation = "moveRight 0.5s linear forwards";
  }

  if (document.documentElement.scrollTop > 2100) {
    document.getElementById("a_awards1").style.animation = "moveToRight 0.5s linear forwards";
  }

  if (document.documentElement.scrollTop > 2400) {
    document.getElementById("a_awards2").style.animation = "moveToRight 0.5s linear forwards";
  }

  if (document.documentElement.scrollTop > 3000) {
    document.getElementById("CS").style.animation = "moveToLeft 1s linear forwards";
  }
  if (document.documentElement.scrollTop > 3200) {
    document.getElementById("Jf").style.animation = "moveToRight 1s linear forwards";
  }
  if (document.documentElement.scrollTop > 3200) {
    document.getElementById("Mixer").style.animation = "moveToLeft 1s linear forwards";
  }
}